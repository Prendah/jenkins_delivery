#!/opt/chefdk/embedded/bin/ruby
require 'mixlib/cli'
require 'ridley'

Ridley::Logging.logger.level = Logger.const_get 'ERROR'

class ChefSearch
  include Mixlib::CLI

  option :chef_config_file,
         short: '-c FILE',
         long: '--chef_config_file FILE',
         required: true

  option :query,
         short: '-q QUERY',
         long: '--query QUERY',
         required: true,
         description: 'Knife search query'
end

cli = ChefSearch.new
cli.parse_options

ridley    = Ridley.from_chef_config(cli.config[:chef_config_file], ssl: { verify: false })

nodes = ridley.search(:node, cli.config[:query]).map(&:chef_attributes)

hostnames = nodes.map { |n| n['cloud_v2']['local_hostname'] }

hostnames.sort!
hostnames.each { |name| puts name }
