import groovy.json.JsonSlurperClassic
import java.time.Instant

/**
 * Wrapper around execute method with all output suppressed
 */
HashMap silentExecute(String environment, List nodeList, String command, HashMap passThroughArgs = [:]) {
  passThroughArgs['output'] = 'none'

  execute(environment, nodeList, command, passThroughArgs)
}

/**
* Runs a command on a list of nodes
*
* @param environment      String  The Chef environment this command should run in
* @param nodeList         List    List of hostnames to run command on
* @param command          String  Command to run on each node in nodeList
* @param passThroughArgs  Map     List of arguments to pass through to the underlying ruby script
*
* @return Map that contains each hostname and the result of the command.
*
* Example return value;
* [
*   node.fqdn: [
*     command: 'hostname',
*     exit_code: 0,
*     start_time: <unix time (seconds since 1970-01-01)>,
*     end_time: <unix time (seconds since 1970-01-01)>,
*     formatted_time_taken: <string representation of end_time - start_time>,
*     stdout: 'node.fqdn',
*     stderr: '',
*     error: ''
*   ]
* ]
*
* passThroughArgs may contain the following entries
*   concurrency       Int     Number of servers to run the command on at once
*   username          String  User to run command from. Default knife
*   identity_file     String  Path of .pem private key for user. Default to /opt/jenkins/knife.pem if no user is passed
*   password          String  SSH password for user. This will be hidden in an environment variable
*   output            String  One of 'reatime', 'grouped' or 'none'. Grouped will only print command output when the command finishes running
*   json_output_file  String  file name to write machine readable results. Contains each hostname and its stdout, stderr and exit code
*/
HashMap execute(String environment, List nodeList, String command, HashMap passThroughArgs = [:]) {
  HashMap output = new HashMap()
  List args  = []
  List environmentVariables = ["NODE_LIST=${nodeList.join("\n")}"]

  passThroughArgs['command'] = command

  if (passThroughArgs.containsKey('password')) {
    environmentVariables << "PASSWORD=${passThroughArgs.remove('password')}"
  }

  if (!passThroughArgs.containsKey('username')) {
    passThroughArgs['username'] = 'knife'
    passThroughArgs['identity_file'] = '/opt/jenkins/.ssh/knife_ssh'
  }

  for (def arg in passThroughArgs) {
    if (arg.value instanceof Boolean) {
      def name = arg.key
      if (!arg.value) {
        name = "no-${name}"
      }

      args << "--${name}"
    } else {
      args << "--${arg.key} '${arg.value}'"
    }
  }

  node("master") {
      withEnv(environmentVariables) {
        wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm', 'default': 1, 'defaultBg': 2]) {
          sh("/opt/jenkins/chef/multissh/execute.rb ${args.join(' ')} --json_output_file nodes_output.txt")
          echo ("Ran multissh")
        }
        rawOutput = readFile('nodes_output.txt')
        output    = parseJson(rawOutput)
        sh("rm -f nodes_output.txt")
        echo("Removed node output")
      }
  }
  echo("Return output for execute")
  return output
}

/**
 * Finds all nodes matching a Knife search query
 *
 * @param environment  String  Data center where this should be ran from
 * @param query        String  Knife search query
 *
 * @return List hostnames matching query
 */
List search(String environment, String query) {
  List found = []

  node("master") {
    ws {
        String server = Platform.environmentToServer(environment)
        sh("/opt/jenkins/chef/multissh/search.rb --chef_config_file '/opt/jenkins/orgs/${server}/${environment}-knife.rb' --query '${query}' > nodes_found.txt")
        found = readFile('nodes_found.txt').readLines()
        sh("rm -f nodes_found.txt")
      }
  }

  return found.sort()
}

HashMap runChef(String environment, HashMap passThroughArgs = [:]) {
  String query = getSearchQuery("name:*", [])
  runChef(environment, query, passThroughArgs)
}

HashMap runChef(String environment, String searchQuery, HashMap passThroughArgs = [:]) {
  HashMap branches = [:]
  HashMap results  = [:]
  Number nodesFound = 0

  // Do not print stdout/stderr. Instead we just want so see when the command is ran and exited
  passThroughArgs['command_output'] = false

  // Create functions to run chef client on all linux and windows servers in a Chef environment.
  // Use the `parallel` workflow step to execute these functions at the same time.
  loop([
    'platform_family:rhel': 'sudo chef-client --no-color'
  ]) { searchAddition, command ->
    // Bind the loop variables to local variables. This
    // is needed when referencing variables in closures.
    String localSearchQuery = searchAddition
    String localCommand     = command

    if (!searchQuery.isEmpty()) {
      localSearchQuery = "${localSearchQuery} AND ${searchQuery}"
    }

    List nodeList = search(environment, localSearchQuery)
    if (nodeList.isEmpty()) {
      echo("No nodes found in ${environment}. Query: ${localSearchQuery}")
      return
    }

    nodesFound += nodeList.size()

    branches[searchAddition] = {
      results << execute(environment, nodeList, localCommand, passThroughArgs)
      echo("finished parallel execute for ${searchAddition}")
    }
  }
  
  if (branches.isEmpty()) {
    error("No nodes found to run chef on")
  }

  currentBuild.setDescription("Running on ${nodesFound} nodes")

  parallel(branches)
  return results
}


// The Collection methods (count, any, findAll etc) method doesn't work within Workflow for some reason
// Wrapping calls to those methods in a @NonCPS annotated method is a workaround

/**
 * Returns true if any host in supplied results didn't exit successfully
 *
 * @param results  Map  Result from execute method
 *
 * @return Boolean true if at least one host failed the exit successfully.
 */
@NonCPS
Boolean anyErrors(HashMap results) {
  results.any({ result ->
    result.value.exit_code != 0
  })
}

/**
 * Returns a string describing the number of successes and failures for the supplied results
 *
 * @param results  Map  Result from execute method
 *
 * @return String example: "Ran on 10 nodes. 5 successful and 5 errors"
 */
@NonCPS
String resultSummary(HashMap results) {
  Number total_hosts = results.size()

  if (total_hosts == 0) {
    return "Ran on 0 nodes"
  }

  Number successes   = results.count({ result -> result.value.exit_code == 0 })
  Number failures    = results.count({ result -> result.value.exit_code != 0 })
  "Ran on ${total_hosts} node${(total_hosts > 1) ? 's' : ''}. ${successes} successful and ${failures} error${(failures > 1) ? 's' : ''}"
}

/**
 * Turns a Knife search query and list of exclusions into another search query.
 * More than one job accepts these inputs. This function allows those jobs to build a consistent search query.
 *
 * @param search      String  Base search query
 * @param exclusions  List    List of exclusions
 *
 * @return String search and exclusions joined together
 */
String getSearchQuery(String search, List exclusions) {
  String query
  if (search.isEmpty()) {
    query = 'name:*'
  } else {
    query = "(${search})"
  }

  if (exclusions.size() > 0) {
    query += " AND NOT (${exclusions.join(' OR ')})"
  }

  return query
}

String getSearchQuery(String search, String exclusions) {
  getSearchQuery(search, exclusions.readLines())
}

/**
 * Parse and return some JSON text
 *
 * @param text        String  Text to parse as JSON
 *
 * @return json       HashMap    
 */
@NonCPS
HashMap parseJson(text) {
  def json = new JsonSlurperClassic().parseText(text)
  return new HashMap(json)
}

/**
 * Accepts a Map of results generated by execute method and archives them.
 * This allows a user to manually or programatically download the archives associated with a run
 *
 * @param results  Map     Results from execute method. Should look like [hostname: [stdout: '', stderr: '', exit_code: 0, command: '']]
 * @param callback Closure Function to run after artifacts have been written. Use this when wanting to do custom things with the artifacts on disk, or example to attach in an email.
 * @return void
 */
void archiveResults(HashMap results, Closure callback = {}) {
  node {
    dir(pwd(tmp: true)) {
      sh 'rm -rf *'
      loop(results) { hostname, data ->
        String contents = resultTemplate(hostname, data)
        String fileName = "${(data.exit_code == 0) ? 'success' : 'failure'}/${hostname}.txt"
        writeFile(file: fileName, text: contents)
        step($class: 'ArtifactArchiver', artifacts: fileName)
      }

      callback.call()
    }
  }
}

/**
 * Turns an entry in results Map from execute method into a string
 * Used when archiving the results.
 *
 * @param name  String  Node name
 * @param node  Map     Entry from return value of execute method
 */
private String resultTemplate(String name, HashMap node) {
  String output = """
Hostname: ${name}
Command: ${node.command}
Exit Code: ${node.exit_code}

Time Taken: ${node.formatted_time_taken}
Start Time: ${Instant.ofEpochSecond(node.start_time)}
End Time: ${Instant.ofEpochSecond(node.end_time)}

Standard Error:
${node.stderr.trim()}

Standard Out:
${node.stdout.trim()}
  """.trim()

  if (node.error) {
    output += """

Command Error (error when running the command, not stderr):
${node.error.trim()}
    """
  }

  output
}

