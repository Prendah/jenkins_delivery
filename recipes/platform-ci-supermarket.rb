node.override['system']['short_hostname'] = 'chef-supermarket'
include_recipe "sbg_jenkins_delivery::_dev-tools"

%w(fog).each do |p|
  chef_gem p do
    options '--no-user-install'
    action :install
  end
end

if ::File.exists?('/home/knife/supermarket.json')
  require 'json'
  file = File.read('/home/knife/supermarket.json')
  config = JSON.parse(file)
  node.override['supermarket_omnibus']['chef_oauth2_app_id'] = config['uid']
  node.override['supermarket_omnibus']['chef_oauth2_secret'] = config['secret']
  node.override['supermarket_omnibus']['chef_oauth2_verify_ssl'] = false
  node.override['supermarket_omnibus']['chef_server_url'] = "https://chef-server.#{node['route53_domain']}"
else 
  node.default['supermarket_omnibus']['chef_oauth2_app_id'] = ''
  node.default['supermarket_omnibus']['chef_oauth2_secret'] = ''
end

unless node['build_number'].to_i > 0
  selinux_state "SELinux Permissive" do
    action :permissive
  end
end

system_hostname node['system']['short_hostname']  do
  short_hostname node['system']['short_hostname']
  domain_name node['system']['domain_name']
  static_hosts (({}))
end

include_recipe "resolver::default"

route53_record "Create #{node['system']['short_hostname']} Route53 Record" do
  name  "#{node['system']['short_hostname']}.#{node['route53_domain']}."
  value lazy { node['cloud_v2']['public_ipv4'] }
  type  "A"
  ttl 60
  zone_id node['route53_zoneid']
  overwrite true
  action :create
  not_if { node['build_number'].to_i > 0 }
end

route53_record "Create jenkins-delivery Route53 Record" do
  name  "#{node['system']['short_hostname']}-int.#{node['route53_domain']}."
  value lazy { node['cloud_v2']['local_ipv4_addrs'] }
  type  "A"
  ttl 60
  zone_id node['route53_zoneid']
  overwrite true
  action :create
  not_if { node['build_number'].to_i > 0 }
end

include_recipe "sbg_jenkins_delivery::knife-target"
include_recipe "supermarket-omnibus-cookbook::default"
include_recipe "sbg_jenkins_delivery::platform-ci-chef-client"
