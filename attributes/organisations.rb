require 'chef'


def get_orgs()
  orgs = {'all' => [ ]}

  Dir.glob("#{node['jenkins']['master']['chef_orgs']}/*-org.json") do |org_conf|
    org_hash = JSON.parse(File.read(org_conf))

    orgs['all'] << org_hash['chef-org']
    orgs[org_hash['chef-api']] = []
    orgs[org_hash['chef-api']] << org_hash['chef-org']
  end

  orgs
end

orgs = get_orgs

orgs.each_pair do |site, o|
  default['sbg_jenkins_delivery']['organisations'][site] = o.sort.uniq
end

