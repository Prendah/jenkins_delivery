require 'serverspec'
require 'json'

set :backend, :exec


describe 'Platform CI Jenkins Slave' do

  $node = ::JSON.parse(File.read('/tmp/kitchen/chef_node.json'))

  %w(make gcc git chefdk).each do |pkg|
    describe package("#{pkg}") do
      it { should be_installed }
    end
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/.aws.properties") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 600 }
    it { should contain 'aws_access_key_id' }
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/.ssh/jenkins") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 600 }
    it { should contain 'BEGIN RSA PRIVATE KEY' }
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/.ssh/jenkins-ssh-key.pub") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 600 }
    it { should contain 'ssh' }
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/.ssh/config") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 600 }
    it { should contain 'IdentityFile' }
  end
  
end

