require 'serverspec'
require 'json'

set :backend, :exec


describe 'sbg_platform_jenkins' do

  $node = ::JSON.parse(File.read('/tmp/kitchen/chef_node.json'))

  %w(jenkins nginx make gcc git chefdk).each do |pkg|
    describe package("#{pkg}") do
      it { should be_installed }
    end
  end

describe file("#{$node["default"]['jenkins']['master']['vco_scripts']}add-dns.rb") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 744 }
  end

describe file("#{$node["default"]['jenkins']['master']['vco_scripts']}delete-dns.rb") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 744 }
  end

describe file("#{$node["default"]['jenkins']['master']['vco_scripts']}build-server.rb") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 744 }
  end

describe file("#{$node["default"]['jenkins']['master']['vco_scripts']}delete-server.rb") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 744 }
  end

describe file("#{$node["default"]['jenkins']['master']['home']}/hudson.plugins.warnings.WarningsPublisher.xml") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should contain 'WarningsDescriptor' }
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/hudson.plugins.emailext.ExtendedEmailPublisher.xml") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should contain 'ExtendedEmailPublisherDescriptor' }
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/.ssh/jenkins") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 600 }
    it { should contain 'BEGIN RSA PRIVATE KEY' }
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/.ssh/jenkins-ssh-key.pub") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 600 }
    it { should contain 'ssh' }
  end

  describe file("#{$node["default"]['jenkins']['master']['home']}/.ssh/config") do
    it { should be_file }
    it { should be_owned_by 'jenkins' }
    it { should be_grouped_into 'jenkins' }
    it { should be_mode 600 }
    it { should contain 'IdentityFile' }
  end
  
end

