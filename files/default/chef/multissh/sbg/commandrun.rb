require 'json'

module Sbg
  # Contains state relating to the execution of a command on a node
  # Note that this doesn't run the command, just stores state
  class CommandRun
    attr_accessor :error
    attr_reader :hostname
    attr_reader :command
    attr_reader :time_taken
    attr_reader :exit_code

    def initialize(hostname, command)
      @hostname = hostname
      @command  = command
      @stdout   = []
      @stderr   = []
    end

    def start
      # Only set start_time if it hasn't already been set
      @start_time ||= Time.now.to_i
    end

    def exit(exit_code)
      # Ensure the start_time has been set
      start

      @exit_code    = exit_code
      @end_time     = Time.now.to_i
      @time_taken   = @end_time - @start_time
    end

    # Returns a human readable string decribing how long the command
    # took to run.
    def formatted_time_taken
      return 'Command has not started yet' if @start_time.nil?
      return 'Command is still running' if @time_taken.nil?

      secs  = @time_taken.to_i
      mins  = secs / 60
      hours = mins / 60

      if hours > 0
        "#{hours} hours and #{mins % 60} minutes"
      elsif mins > 0
        "#{mins} minutes and #{secs % 60} seconds"
      elsif secs >= 0
        "#{secs} seconds"
      end
    end

    def to_h
      {
        command: @command,
        stdout: @stdout.join("\n"),
        stderr: @stderr.join("\n"),
        start_time: @start_time,
        end_time: @end_time,
        formatted_time_taken: formatted_time_taken,
        exit_code: @exit_code,
        error: @error
      }
    end

    def to_json(*options)
      JSON.generate(to_h.to_json(*options))
    end

    def append_stdout(stdout)
      @stdout += stdout.split("\n") unless stdout.nil?
    end

    def append_stderr(stderr)
      @stderr += stderr.split("\n") unless stderr.nil?
    end
  end
end
