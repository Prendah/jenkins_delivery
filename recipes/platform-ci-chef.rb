include_recipe "chef-vault"
include_recipe "resolver::default"
include_recipe "sbg_jenkins_delivery::knife-target"
include_recipe "sbg_jenkins_delivery::_dev-tools"

node.override['system']['short_hostname'] = 'chef-server'

%w(fog).each do |p|
  chef_gem p do
    options '--no-user-install'
    action :install
  end
end

unless node['build_number'].to_i > 0
  selinux_state "SELinux Permissive" do
    action :permissive
  end
end

system_hostname node['system']['short_hostname']  do
  short_hostname node['system']['short_hostname']
  domain_name node['system']['domain_name']
  static_hosts (({}))
end 

route53_record "Create #{node['system']['short_hostname']} Route53 Record" do
  name  "#{node['system']['short_hostname']}.#{node['route53_domain']}."
  value lazy { node['cloud_v2']['public_ipv4'] }
  type  "A"
  ttl 60
  zone_id node['route53_zoneid']
  overwrite true
  action :create
  not_if { node['build_number'].to_i > 0 }
end

route53_record "Create #{node['system']['short_hostname']} Route53 Internal Record" do
  name  "#{node['system']['short_hostname']}-int.#{node['route53_domain']}."
  value lazy { node['cloud_v2']['local_ipv4_addrs'] }
  type  "A"
  ttl 60
  zone_id node['route53_zoneid']
  overwrite true
  action :create
  not_if { node['build_number'].to_i > 0 }
end


include_recipe "chef-server::default"
include_recipe "chef-server::addons"

file "/home/knife/supermarket.json" do
  owner 'root'
  group 'root'
  mode 0644
  content lazy {::File.exists?("/etc/opscode/oc-id-applications/supermarket.json") ? ::File.read("/etc/opscode/oc-id-applications/supermarket.json") : ''}
  action :create
end

include_recipe "sbg_jenkins_delivery::platform-ci-chef-client"
