%w(gcc make).each do |p|
  yum_package p do
    action :nothing
  end.run_action(:install)
end
