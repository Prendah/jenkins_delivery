resource_name :jenkins_job

property :password, [String, nil], default: node['jenkins']['master']['api_pass']
property :path, String, required: false, default: ''
property :job_params, Hash, required: false, default: {}
property :job_opts, Hash, required: false, default: {}

load_current_value do
  if ::File.exist? '/opt/jenkins/secrets/initialAdminPassword'
    password IO.read('/opt/jenkins/secrets/initialAdminPassword').chomp
  end
end

action :create do
  require 'jenkins_api_client'
  client = JenkinsApi::Client.new( :server_ip => '0.0.0.0',
                                   :username => node['jenkins']['master']['api_user'],
                                   :password => password)
  client.job.create_or_update(current_resource.name, IO.read(path))
end

action :build do
  require 'jenkins_api_client'
  client = JenkinsApi::Client.new( :server_ip => '0.0.0.0',
                                   :username => node['jenkins']['master']['api_user'],
                                   :password => password)
  client.job.build(current_resource.name, params=job_params, opts = job_opts)
end
