default['chef-server']['addons'] = ["manage"]

default['chef-server']['configuration'] = "
oc_id['applications'] = {
  'supermarket' => {
    'redirect_uri' => 'https://chef-supermarket.#{node['route53_domain']}/auth/chef_oauth2/callback'
  }
}

"


default['chef-server']['api_fqdn'] = "chef-server-int.#{node['route53_domain']}"
default['chef-server']['accept_license'] = true
default['supermarket_omnibus']['chef_server_url'] = "https://chef-server.#{node['route53_domain']}"

default['chef-server']['addons'] = ["manage"]
