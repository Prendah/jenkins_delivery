resource_name :jenkins_system

property :password, [String, nil], default: node['jenkins']['master']['api_pass']

load_current_value do
  sleep 30
  if ::File.exist? '/opt/jenkins/secrets/initialAdminPassword'
    password IO.read('/opt/jenkins/secrets/initialAdminPassword').chomp
  end
end

action :wait_for_ready do
  require 'jenkins_api_client'
  client = JenkinsApi::Client.new( :server_ip => '0.0.0.0',
                                   :username => node['jenkins']['master']['api_user'],
                                   :password => password)
  system = JenkinsApi::Client::System.new(client)
  system.wait_for_ready
end
