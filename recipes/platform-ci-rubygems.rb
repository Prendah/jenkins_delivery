# Install the ChefDK and some rubygems we need

include_recipe "sbg_jenkins_delivery::_dev-tools"

chef_dk 'Install ChefDK' do
    action :install
end

%w(kitchen-docker kitchen-sync knife-ec2 colorize trollop knife-opc knife-acl httparty wopen3 sinatra filelock thread fog knife-supermarket).each do |p|
  gem_package p do
    gem_binary "/opt/chefdk/embedded/bin/gem"
    options '--no-user-install'
    action :install
  end
end

# Install the chef gems

%w(jenkins_api_client fog).each do |p|
  chef_gem p do
    options '--no-user-install'
    action :install
  end
end
