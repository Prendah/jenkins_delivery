%w{perl-JSON perl-ExtUtils-MakeMaker zlib-devel openssl-devel cpio expat-devel gettext-devel gcc autoconf make libcurl-devel unzip tmux}.each do |pkg|
  yum_package pkg do
    action :install
    allow_downgrade true
  end
end


# Lets install a newer GIT!

installer_dir = "/tmp/git"
git_version = node['sbg_jenkins_delivery']['git_version']
git_mirror = node['sbg_jenkins_delivery']['git_url']

git_tarball_src = "#{git_mirror}/git-#{git_version}.tar.gz"

directory "#{installer_dir}/src" do
  recursive true
end

remote_file "#{installer_dir}/src/git-#{git_version}.tgz" do
  source git_tarball_src
  checksum node['sbg_jenkins_delivery']['checksum']
  user "root"
  group "root"
  notifies :run, "execute[extract]", :immediately
end

execute "extract" do
  cwd "#{installer_dir}/src"
  command "tar xvf git-#{git_version}.tgz"
  action :nothing
end

execute "Make git ./configure" do
  cwd "#{installer_dir}/src/git-#{git_version}"
  command "make configure"
  creates "#{installer_dir}/src/git-#{git_version}/configure"
  user "root"
  group "root"
  action :run
  not_if {File.exists?("#{installer_dir}/src/git-#{git_version}/configure")}
end

execute "Compile and install new git version" do
  cwd "#{installer_dir}/src/git-#{git_version}"
  command "./configure --prefix=/usr/local && make && make install"
  creates "/usr/local/bin/git"
  user "root"
  group "root"
  action :run
  not_if {`perl -e 'if (-e "/usr/local/bin/git") { print system("/usr/local/bin/git --version");} else { print "Not present\n";}'`.include? git_version}
  notifies :run, "execute[Compile and install subtree]", :immediately
end

execute "Compile and install subtree" do
  cwd "#{installer_dir}/src/git-#{git_version}/contrib/subtree"
  command "make && make install"
  user "root"
  group "root"
  action :nothing
end
