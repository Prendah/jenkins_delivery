
node.override['chef_client']['config']['ssl_verify_mode'] = ":verify_peer"
node.override['chef_client']['config']['chef_server_url'] = "https://chef-server-int.#{node['route53_domain']}/organizations/#{node.chef_environment}"
node.override['chef_client']['config']['node_name'] = "#{node['system']['short_hostname']}.#{node['route53_domain']}"
node.override['chef_client']['config']['validation_client_name'] = "#{node.chef_environment}-validator"
node.override['chef_client']['config']['chef_environment'] = node.chef_environment
node.override['chef_client']['config']['log_level'] = ":info"

cmd = "curl --max-time 5 -k https://jenkins-delivery-int.#{node['route53_domain']}/validators/#{node['chef-server']['api_fqdn']}/management.pem > /etc/chef/validation.pem"

execute 'Install validation.pem' do
  command cmd
  ignore_failure true
  not_if { ::File.exists?('/etc/chef/client.pem') }
end

directory "/etc/chef/trusted_certs" do
  action :create
  mode "0644"
  owner "root"
  group "root"
end

cmd = "openssl s_client -connect #{node['chef-server']['api_fqdn']}:443 -showcerts </dev/null| openssl x509 -outform PEM > /etc/chef/trusted_certs/#{node['chef-server']['api_fqdn'].gsub( /\./, "_" )}.crt"

execute 'Fetch Chef API certificate' do
  command cmd
  not_if { ::File.exists?("/etc/chef/trusted_certs/#{node['chef-server']['api_fqdn'].gsub( /\./, "_" )}.crt") }
end

include_recipe 'chef-client::config'

ruby_block "Save node JSON" do
  block do
    if Dir::exist?('/etc/chef')
      IO.write("/etc/chef/chef_node.json", node.to_json)
    end
  end
end
