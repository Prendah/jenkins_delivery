require 'io/wait'

module Sbg
  # Utility methods to deal with CLI related activities
  module CLI
    # Reads lines from a pipe (ie STDIN) and yields when no data is left.
    #
    # Useful when dealing with streaming data, this method may yield many times
    # as data is written to the pipe.
    #
    # Sbg::CLI.read_lines_from_pipe(STDIN) do |lines|
    #   puts "Got #{lines.join("\n")} from STDIN"
    # end
    def read_lines_from_pipe(fd)
      loop do
        break if fd.tty? || fd.eof?

        fd.wait_readable

        lines = []
        loop do
          break if !fd.ready? || fd.eof?
          lines << fd.readline.strip
        end

        yield lines
      end
    end
  end
end
