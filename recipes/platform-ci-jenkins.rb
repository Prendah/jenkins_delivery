
node.override['system']['short_hostname'] = 'jenkins-delivery'
node.override['nginx']['repo_source'] = 'nginx'

include_recipe "sbg_jenkins_delivery::java"
include_recipe "yum-epel"
include_recipe "resolver::default"
include_recipe "sbg_jenkins_delivery::platform-ci-rubygems"
include_recipe "sbg_jenkins_delivery::install-git"
include_recipe "sbg_jenkins_delivery::platform-ci-docker"
include_recipe "sbg_jenkins_delivery::_dev-tools"

unless node['build_number'].to_i > 0
  selinux_state "SELinux Permissive" do
    action :permissive
  end
end

system_hostname node['system']['short_hostname']  do
  short_hostname node['system']['short_hostname']
  domain_name node['system']['domain_name']
  static_hosts (({}))
end

yum_package "nginx" do
  action :install
end

directory "/etc/nginx/ssl" do
  action :create
  owner 'root'
  group 'root'
  mode '0750'
end

ssl_certificate "jenkins-delivery.#{node['route53_domain']}" do
  notifies :restart, 'service[nginx]'
  dir "/etc/nginx/ssl"
end

template "/etc/nginx/conf.d/jenkins.conf" do
  source "nginx-jenkins.conf.erb"
  action :create
  owner 'root'
  group 'root'
  mode 0644
  notifies :restart, "service[nginx]", :immediately
end

service "nginx" do
  action [:start, :enable]
end

route53_record "Create jenkins-delivery Route53 Record" do
  name  "jenkins-delivery.#{node['route53_domain']}."
  value lazy { node['cloud_v2']['public_ipv4'] }
  type  "A"
  ttl 60
  zone_id node['route53_zoneid']
  overwrite true
  action :create
  not_if { node['build_number'].to_i > 0 }
end

route53_record "Create jenkins-delivery Route53 Internal Record" do
  name  "jenkins-delivery-int.#{node['route53_domain']}."
  value lazy { node['cloud_v2']['local_ipv4_addrs'] }
  type  "A"
  ttl 60
  zone_id node['route53_zoneid']
  overwrite true
  action :create
  not_if { node['build_number'].to_i > 0 }
end

jenkins_pkg_src = "#{node['jenkins']['master']['repository']}/jenkins-#{node['jenkins']['master']['version']}.noarch.rpm"
installer_dir = "/tmp/jenkins"

directory "#{installer_dir}/pkg" do
  recursive true
end

remote_file "#{installer_dir}/pkg/jenkins-#{node['jenkins']['master']['version']}.noarch.rpm" do
  source jenkins_pkg_src
  checksum node['jenkins']['master']['package_checksum']
  user "root"
  group "root"
  notifies :install, "rpm_package[jenkins]", :immediately
end

rpm_package "jenkins" do
  allow_downgrade true
  source "#{installer_dir}/pkg/jenkins-#{node['jenkins']['master']['version']}.noarch.rpm"
  action [:install]
end

directory node['jenkins']['master']['home'] do
  action :create
  user node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode '0750'
end

template '/etc/sysconfig/jenkins' do
  source   'jenkins-config-rhel.erb'
  mode     '0644'
  notifies :restart, 'service[jenkins]', :immediately
end

cookbook_file "#{node['jenkins']['master']['home']}/scriptApproval.xml" do
  source "jenkins/scriptApproval.xml"
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode "0640"
end

cookbook_file "#{node['jenkins']['master']['home']}/org.jenkinsci.main.modules.sshd.SSHD.xml" do
  source "jenkins/org.jenkinsci.main.modules.sshd.SSHD.xml"
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode "0640"
end

file "#{node['jenkins']['master']['home']}/jenkins.install.InstallUtil.lastExecVersion" do
  content '2.8'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode "0640"
end

service 'jenkins' do
  supports status: true, restart: true, reload: true
  action  [:enable, :start]
  notifies :wait_for_ready, 'jenkins_system[wait for jenkins startup]', :immediately
end

jenkins_system "wait for jenkins startup" do
  action :nothing
end

directory "#{node['jenkins']['master']['home']}/managed-conf" do
  action :create
  owner "jenkins"
  group "jenkins"
  mode "0750"
end

jenkins_plugins "Install Plugins"  do
  plugins node['jenkins']['master']['install_plugins']
  action :install
end

template "#{node['jenkins']['master']['home']}/.gitconfig" do
  source "gitconfig.erb"
  action :create
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode "0640"
end

directory "/var/lib/jenkins/.ssh" do
  action :create
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode "0750"
end

template "/var/lib/jenkins/.ssh/config" do
  source "ssh.erb"
  action :create
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode "0640"
end

template "/etc/profile.d/delivery.sh" do
  source "delivery.sh.erb"
  action :create
  owner 'root'
  group 'root'
  mode "0755"
end

cb = run_context.cookbook_collection["sbg_jenkins_delivery"]
cb.manifest['templates'].each do |t|
  if t['path'].eql? "templates/default/platform-ci/#{t['name']}"
    config_path = "#{node['jenkins']['master']['home']}/managed-conf"

    jobname = t['name'].match(/(.*)\.xml\.erb/).captures[0]
    filename = t['name'].match(/(.*\.xml)\.erb/).captures[0]
    template "#{config_path}/#{filename}" do
      source "platform-ci/#{t['name']}"
      action :create
      owner "root"
      group node['jenkins']['master']['user']
      mode "0640"
      variables({
        :public_ssh => data_bag_item('keys', 'git')['git_public']
      })
    end

    jenkins_job jobname do
      path  "#{config_path}/#{filename}"
      action :create
    end
  end
end

directory "#{node['jenkins']['master']['home']}/.ssh" do
  action :create
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0750
end

file "#{node['jenkins']['master']['home']}/.ssh/id_rsa" do
  content data_bag_item('keys', 'git')['git_private']
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0600
end

file "#{node['jenkins']['master']['home']}/.ssh/id_rsa.pub" do
  content data_bag_item('keys', 'git')['git_public']
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0600
end

directory node['jenkins']['master']['chef_orgs'] do 
  action :create
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0750
end

directory node['jenkins']['master']['validators'] do
  action :create
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0755
end

ssh_known_hosts_entry 'github.com'
ssh_known_hosts_entry 'bitbucket.org'

node['jenkins']['master']['chef_servers'].each do |s|

  ssh_known_hosts_entry s

  directory "#{node['jenkins']['master']['chef_orgs']}/#{s}" do
    action :create
    recursive true
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['user']
    mode 0750
  end
  directory "#{node['jenkins']['master']['chef_users']}/#{s}" do
    action :create
    recursive true
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['user']
    mode 0750
  end
  directory "#{node['jenkins']['master']['validators']}/#{s}" do
    action :create
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['user']
    mode 0755
  end
end

ruby_block 'Configure Knife and Berkshelf configurations' do
  block do
    require 'json'
    Dir.glob("#{node['jenkins']['master']['chef_orgs']}/*-org.json") do |org_conf|
      org_hash = JSON.parse(File.read(org_conf))
      t = Chef::Resource::Template.new( "#{node['jenkins']['master']['chef_orgs']}/#{org_hash['chef-api']}/#{org_hash['chef-org']}-knife.rb", run_context)
      t.cookbook "sbg_jenkins_delivery"

      t.source "knife.rb.erb"
      t.owner node['jenkins']['master']['user']
      t.group node['jenkins']['master']['user']
      t.mode "0640"
      t.variables({
        :admin_usr => org_hash['admin-user'],
        :chef_server => org_hash['chef-api'],
        :chef_env => org_hash['chef-org']
      })
      t.run_action :create

      b = Chef::Resource::Template.new("#{node['jenkins']['master']['chef_orgs']}/#{org_hash['chef-api']}/#{org_hash['chef-org']}-berks.json", run_context)
      b.cookbook "sbg_jenkins_delivery"
      b.source "berks.json.erb"
      b.owner node['jenkins']['master']['user']
      b.group node['jenkins']['master']['user']
      b.mode "0644"
      b.variables ({
        :admin_usr => org_hash['admin-user'],
        :chef_server => org_hash['chef-api'],
        :chef_env => org_hash['chef-org']
      })
      b.run_action :create
    end
  end
  action :run
end 

directory "#{node['jenkins']['master']['home']}/chef" do
  action :create
  recursive true
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0750
end

%w(cookbook-upload.rb role-upload.rb logger.rb environment-upload.rb data-bag-upload.rb).each do |s|
  cookbook_file "#{node['jenkins']['master']['home']}/chef/#{s}" do
    source "chef/#{s}"
    action :create
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['user']
    mode 0750
  end
end

directory "#{node['jenkins']['master']['home']}/chef/multissh/sbg" do
  action :create
  recursive true
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0750
end

%w(execute.rb search.rb).each do |s|
  cookbook_file "#{node['jenkins']['master']['home']}/chef/multissh/#{s}" do
    source "chef/multissh/#{s}"
    action :create
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['user']
    mode 0750
  end
end

%w(cli.rb  commandrun.rb  multissh.rb).each do |s|
  cookbook_file "#{node['jenkins']['master']['home']}/chef/multissh/sbg/#{s}" do
    source "chef/multissh/sbg/#{s}"
    action :create
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['user']
    mode 0750
  end
end

directory "#{node['jenkins']['master']['home']}/workflow-libs/vars" do
  action :create
  recursive true
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0750
end

%w(ChefNodes.groovy  ChefNodes.txt  JenkinsBuild.groovy  loop.groovy  Platform.groovy).each do |s|
  cookbook_file "#{node['jenkins']['master']['home']}/workflow-libs/vars/#{s}" do
    source "jenkins/workflow-libs/#{s}"
    action :create
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['user']
    mode 0750
  end
end

group "docker" do
  action :modify
  members 'jenkins'
  append true
  notifies :restart, "service[jenkins]", :immediately
end

file "#{node['jenkins']['master']['home']}/.ssh/#{node['sbg_jenkins_delivery']['aws_ssh_key_id']}" do
  content data_bag_item('keys', 'aws')['aws_private']
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0750
end

include_recipe "sbg_jenkins_delivery::knife-user"
include_recipe "sbg_jenkins_delivery::platform-ci-chef-client"

# This job is only intended to be run once on initial configuration of the system

jenkins_job 'InitialSetup' do
  action :build
  not_if { ::File.exists?("#{node['jenkins']['master']['home']}/deliveryInitialSetup") }
end

file "#{node['jenkins']['master']['home']}/deliveryInitialSetup" do
  content ''
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['user']
  mode 0640
end
