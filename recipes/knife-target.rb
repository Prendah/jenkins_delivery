user 'knife' do
  comment 'A user for ssh commands'
  supports :manage_home => true
  home '/home/knife'
  shell '/bin/bash'
  password '*'
end

kkey = data_bag_item('keys', 'knife')['knife_public']

directory '/home/knife/.ssh' do
  action :create
  user 'knife'
  group 'knife'
  mode '0700'
end

file '/home/knife/.ssh/authorized_keys' do
  content kkey
  user 'knife'
  group 'knife'
  mode '0640'
end

directory '/opt/knife' do
  mode '0500'
  owner 'root'
  group 'root'
end

%w(chef-server-ctl.rb chef-client.rb).each do |script|
  template "/opt/knife/#{script}" do
    mode '0500'
    owner 'root'
    group 'root'
    source "knife/#{script}.erb"
  end
end

sudo 'knife' do
  user      "knife"
  runas     'root'
  nopasswd  true
  commands  ['/opt/knife/*', '/usr/bin/scp', '/bin/chef-client', '/usr/bin/ssh', '/bin/cp /tmp/validation.pem /etc/chef/validation.pem', '/bin/rm -f /etc/chef/client.pem']
  defaults  ['!requiretty']
end
